﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlePentecostes.DAL;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ControlePentecostes.BLL
{
    public class ParentescoBO
    {
        public string BuscaParentesco(dynamic param)
        {
            var db = new ControlePentecostesEntities();

            var query = (from p in db.tbl_parentesco select new { p.nm_parentesco });

            string retorno = JsonConvert.SerializeObject(query.ToList(), Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;
        }

        public int RecuperaParentesco(string parentescoSelecionado)
        {
            var db = new ControlePentecostesEntities();

            var query = (from p in db.tbl_parentesco
                                         where p.nm_parentesco.Equals(parentescoSelecionado)
                                         select p).FirstOrDefault();

            if (query == null)
            {
                var parentesco = new tbl_parentesco();
                parentesco.nm_parentesco = parentescoSelecionado;
                db.tbl_parentesco.Add(parentesco);
                db.SaveChanges();

                return parentesco.id_parentesco;
            }
            return query.id_parentesco;
        }
    }
}
