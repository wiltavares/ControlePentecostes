﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlePentecostes.BLL
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AttributeBO : Attribute
    {
        public string ChavePermissao { get; set; }
        public bool ValidarPermissao { get; set; }
        public bool LogarMetodo { get; set; }
        public string[] ListaChavePermissao { get; set; }

        public AttributeBO(string chavePermissao, bool validarPermissao = true, bool logarMetodo = false)
        {
            this.ChavePermissao = chavePermissao;
            this.ValidarPermissao = validarPermissao;
            this.LogarMetodo = logarMetodo;
            this.ListaChavePermissao = null;
        }

        public AttributeBO(bool validarPermissao = true, bool logarMetodo = false)
        {
            this.ChavePermissao = string.Empty;
            this.ValidarPermissao = validarPermissao;
            this.LogarMetodo = logarMetodo;
            this.ListaChavePermissao = null;
        }

        public AttributeBO(bool validarPermissao = true, bool logarMetodo = false, params string[] chavePermissao)
        {
            this.ChavePermissao = null;
            this.ListaChavePermissao = chavePermissao;
            this.ValidarPermissao = validarPermissao;
            this.LogarMetodo = logarMetodo;
        }

        public bool GS1CNPValidaPermissao(List<string> permissao)
        {
            if (ListaChavePermissao != null)
            {
                foreach (var item in permissao)
                {
                    foreach (var item2 in this.ListaChavePermissao)
                    {
                        if (item.Equals(item2.ToString()))
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                foreach (var item2 in permissao)
                {
                    if (this.ChavePermissao.Equals(item2.ToString()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
