﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlePentecostes.BLL
{
    public class Util
    {
        public string ConvertReaderTOJson(IDataReader reader, int pageNumber = 0)
        {
            if (reader != null || !reader.IsClosed)
            {
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                int rowcount = 0,
                    currentPage = 0;

                using (JsonWriter jsonWriter = new JsonTextWriter(sw))
                {
                    jsonWriter.WriteStartArray();

                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();

                        int fields = reader.FieldCount;

                        for (int i = 0; i < fields; i++)
                        {
                            if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                            {
                                jsonWriter.WritePropertyName(reader.GetName(i).ToLower());
                                jsonWriter.WriteValue(reader[i]);
                            }
                            else if (rowcount == 0 && reader.GetName(i).ToUpper().Equals("ROWCOUNT") && reader[i] != null)
                            {
                                rowcount = Int32.Parse(reader[i].ToString());
                            }
                            else if (currentPage == 0 && reader.GetName(i).ToUpper().Equals("CURRENTPAGE") && reader[i] != null)
                            {
                                currentPage = Int32.Parse(reader[i].ToString());
                            }
                        }

                        jsonWriter.WriteEndObject();
                    }

                    jsonWriter.WriteEndArray();
                }

                if (rowcount > 0)
                {
                    return "{\"RowCount\": " + rowcount.ToString() + ", \"CurrentPage\": " + currentPage.ToString() + ", \"data\": " + sw.ToString() + "}";
                }
                else
                {
                    return sw.ToString();
                }
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public object ConvertReaderTOExpandoObject(IDataReader reader)
        {
            if (reader != null || !reader.IsClosed)
            {
                List<ExpandoObject> retorno = new List<ExpandoObject>();

                while (reader.Read())
                {
                    dynamic linha = new ExpandoObject();
                    int fields = reader.FieldCount;

                    for (int i = 0; i < fields; i++)
                    {
                        if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                        {
                            (linha as IDictionary<string, object>).Add(reader.GetName(i), reader.GetValue(i));
                        }
                    }

                    retorno.Add(linha);
                }

                return retorno;
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public static object ConvertJTokenToObject(JToken token)
        {
            if (token is JValue)
            {
                return ((JValue)token).Value;
            }
            if (token is JObject)
            {
                ExpandoObject expando = new ExpandoObject();

                (from childToken in ((JToken)token) where childToken is JProperty select childToken as JProperty).ToList().ForEach(property =>
                {
                    ((IDictionary<string, object>)expando).Add(property.Name, ConvertJTokenToObject(property.Value));
                });

                return expando;
            }
            if (token is JArray)
            {
                object[] array = new object[((JArray)token).Count];
                int index = 0;

                foreach (JToken arrayItem in ((JArray)token))
                {
                    array[index] = ConvertJTokenToObject(arrayItem);
                    index++;
                }

                return array;
            }

            throw new ArgumentException(string.Format("Unknown token type '{0}'", token.GetType()), "token");
        }

    }
}
