﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlePentecostes.DAL;
using Newtonsoft.Json;

namespace ControlePentecostes.BLL
{
    public class GrupinhoBO
    {
        public string BuscaGrupinhos(dynamic param)
        {
            var db = new ControlePentecostesEntities();

            var query = (from p in db.tbl_grupinho select new { p.nm_grupinho });

            string retorno = JsonConvert.SerializeObject(query.ToList(), Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;
        }

        public int recuperaGrupinho(string grupinhoSelecionado)
        {
            var db = new ControlePentecostesEntities();

            var query = (from p in db.tbl_grupinho
                         where p.nm_grupinho.Equals(grupinhoSelecionado)
                         select p).FirstOrDefault();

            if (query == null)
            {
                var grupinho = new tbl_grupinho();
                grupinho.nm_grupinho = grupinhoSelecionado;
                db.tbl_grupinho.Add(grupinho);
                db.SaveChanges();

                return grupinho.id_grupinho;
            }

            return query.id_grupinho;
        }
    }
}
