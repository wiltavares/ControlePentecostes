﻿using ControlePentecostes.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlePentecostes.BLL
{
    public class CriancaBO
    {
        public string BuscaCriancaPorIdCadastro(dynamic param)
        {
            int idCadastro = Convert.ToInt32(param.where);
            var db = new ControlePentecostesEntities();

            var query = (from c in db.tbl_crianca
                         where c.id_cadastro == idCadastro
                         select new 
                         { 
                             c.id_crianca,
                             c.id_cadastro,
                             nome = c.nm_crianca,
                             restricaoSelecionada = c.tbl_restricao_alimentar.nm_restricao_alimentar,
                             grupinhoSelecionado = c.tbl_grupinho.nm_grupinho,
                             c.idade,
                             c.presente,
                             c.filhoDeServo
                         });

            string retorno = JsonConvert.SerializeObject(query.ToList(), Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;
        }

        public string TotalizarCriancas(dynamic param)
        {
            var db = new ControlePentecostesEntities();

            //var query = db.tbl_crianca.Count();
           var query = db.tbl_crianca.Where(c => c.presente == 1).Count();

            string retorno = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;

        }

        public void EntregarCrianca(dynamic param)
        {
            try
            {
                int idCrianca = Convert.ToInt32(param.where.id_crianca);
                var db = new ControlePentecostesEntities();

                var query = db.tbl_crianca.Where(c => c.id_crianca == idCrianca).FirstOrDefault();
                query.presente = 0;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        public void EntregarTodasCriancas(dynamic param)
        {
            try
            {
                int idCadastro = Convert.ToInt32(param.where.id_cadastro);
                var db = new ControlePentecostesEntities();

                var query = db.tbl_crianca.Where(c => c.id_cadastro == idCadastro).ToList();

                foreach (var c in query)
                {
                    c.presente = 0;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void VoltarCrianca(dynamic param)
        {
            try
            {
                int idCrianca = Convert.ToInt32(param.where.id_crianca);
                var db = new ControlePentecostesEntities();

                var query = db.tbl_crianca.Where(c => c.id_crianca == idCrianca).FirstOrDefault();
                query.presente = 1;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
