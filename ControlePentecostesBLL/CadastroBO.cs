﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlePentecostes.DAL;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ControlePentecostes.BLL
{
    public class CadastroBO
    {
        public int Salvar(dynamic param)
        {
            try
            {
                var db = new ControlePentecostesEntities();
                tbl_cadastro cadastroObj = new tbl_cadastro();
                populaDados(cadastroObj, param.where);
                db.tbl_cadastro.Add(cadastroObj);
                db.SaveChanges();

                int idCadastro = cadastroObj.id_cadastro;

                SalvarContatos(param.where.contatos, idCadastro);

                SalvarCriancas(param.where.criancas, idCadastro);

                return cadastroObj.id_cadastro;
            }
            catch (Exception ex)
            {
                throw new Exception("Falha ao salvar registro. "+ex.Message);            }   
            }

        public void Atualizar(dynamic param)
        {
            try
            {
                var db = new ControlePentecostesEntities();
                tbl_cadastro cadastroObj = new tbl_cadastro();
                populaDados(cadastroObj, param.where);
                cadastroObj.id_cadastro = Convert.ToInt32(param.where.id_cadastro);

                var original = db.tbl_cadastro.Where(c => c.id_cadastro == cadastroObj.id_cadastro).FirstOrDefault();
                db.Entry(original).CurrentValues.SetValues(cadastroObj);
                db.SaveChanges();

                AtualizarContatos(param.where.contatos, cadastroObj.id_cadastro);
                AtualizarCriancas(param.where.criancas, cadastroObj.id_cadastro);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SalvarContatos(dynamic param, int idCadastro)
        {
            var db = new ControlePentecostesEntities();
            var tbl = new tbl_contatos();

            foreach (var c in param)
            {
                tbl.contato = c.contato;
                tbl.id_cadastro = idCadastro;
                db.tbl_contatos.Add(tbl);
                db.SaveChanges();
            }       
        }

        public void AtualizarContatos(dynamic param, int idCadastro)
        {
            var db = new ControlePentecostesEntities();
            var tbl = new tbl_contatos();

            var deletar = db.tbl_contatos.Where(c => c.id_cadastro == idCadastro).ToList();

            if (deletar != null)
            {
                db.tbl_contatos.RemoveRange(deletar);
                db.SaveChanges();
            }            

            foreach (var c in param)
            {
                tbl.contato = c.contato;
                tbl.id_cadastro = idCadastro;
                db.tbl_contatos.Add(tbl);
                db.SaveChanges();
            } 
        }

        public void SalvarCriancas(dynamic param, int idCadastro)
        {
            var db = new ControlePentecostesEntities();
            var tbl = new tbl_crianca();

            foreach (var c in param)
            {
                tbl.nm_crianca = c.nome;
                tbl.idade = c.idade;
                tbl.id_restricao_alimentar = (new RestricaoAlimentarBO()).recuperaRestricaoAlimentar(c.restricaoSelecionada.ToString());
                tbl.id_grupinho = (new GrupinhoBO()).recuperaGrupinho(c.grupinhoSelecionado.ToString());
                tbl.presente = c.presente;
                tbl.filhoDeServo = c.filhoDeServo;
                tbl.id_cadastro = idCadastro;

                db.tbl_crianca.Add(tbl);
                db.SaveChanges();
            }
        }

        public void AtualizarCriancas(dynamic param, int idCadastro)
        {
            var db = new ControlePentecostesEntities();
            var tbl = new tbl_crianca();
                        
            var deletar = db.tbl_crianca.Where(c => c.id_cadastro == idCadastro).ToList();

            if (deletar != null)
            {
                db.tbl_crianca.RemoveRange(deletar);
                db.SaveChanges();
            }            

            foreach (var c in param)
            {
                tbl.nm_crianca = c.nome;
                tbl.idade = c.idade;
                tbl.id_restricao_alimentar = (new RestricaoAlimentarBO()).recuperaRestricaoAlimentar(c.restricaoSelecionada.ToString());
                tbl.id_grupinho = (new GrupinhoBO()).recuperaGrupinho(c.grupinhoSelecionado.ToString());
                tbl.presente = c.presente;
                tbl.filhoDeServo = c.filhoDeServo;
                tbl.id_cadastro = idCadastro;

                db.tbl_crianca.Add(tbl);
                db.SaveChanges();
            }
        }

        public void populaDados(tbl_cadastro obj, dynamic param)
        {
            obj.nm_responsavel = param.responsavel;
            obj.informacao_adicional = param.informacaoAdicional;
            obj.id_parentesco = recuperaParentesco(param.parentescoSelecionado.ToString());
        }

        public int recuperaParentesco(string parentescoSelecionado)
        {
            ParentescoBO parentesco = new ParentescoBO();
            return parentesco.RecuperaParentesco(parentescoSelecionado);
            
        }

        public string BuscarCadastroPorId(dynamic param)
        {
            int idCadastro = Convert.ToInt32(param.where);
            var db = new ControlePentecostesEntities();

            var query = (from c in db.tbl_cadastro 
                            where c.id_cadastro == idCadastro
                            select new
                            {
                                 c.id_cadastro,
                                 responsavel = c.nm_responsavel,
                                 informacaoAdicional = c.informacao_adicional,
                                 parentescoSelecionado = c.tbl_parentesco.nm_parentesco
                            }
                        ).FirstOrDefault();

            string retorno = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;
        }

        public string BuscaSelecionada(dynamic param)
        {
            int idCadastro = param.where.id_cadastro != null ? Convert.ToInt32(param.where.id_cadastro) : 0;
            string nmCrianca = param.where.nm_crianca != null ? param.where.nm_crianca.ToString() : String.Empty;
            string nmResponsavel = param.where.nm_responsavel != null ? param.where.nm_responsavel.ToString() : String.Empty;
            int presente = param.where.presente != null ? Convert.ToInt32(param.where.presente) : 0;
            int filhoDeServo = param.where.filhoDeServo != null ? Convert.ToInt32(param.where.filhoDeServo) : 0;

            var db = new ControlePentecostesEntities();

            var query = (from c in db.tbl_crianca
                         where (c.id_cadastro == idCadastro || idCadastro == 0) &
                               (c.nm_crianca.Contains(nmCrianca) || nmCrianca.Equals(String.Empty)) &
                               (c.tbl_cadastro.nm_responsavel.Contains(nmResponsavel) || nmResponsavel.Equals(String.Empty)) &
                               (c.presente == presente || presente == 0) & (c.filhoDeServo == filhoDeServo || filhoDeServo == 0)
                         group c by new { c.id_cadastro, c.tbl_cadastro.nm_responsavel, c.nm_crianca, c.presente, c.filhoDeServo } into g
                         orderby g.Key.id_cadastro, g.Key.nm_responsavel ascending
                         select new
                         {
                             g.Key.id_cadastro,
                             g.Key.nm_responsavel,
                             g.Key.nm_crianca,
                             g.Key.presente,
                             g.Key.filhoDeServo
                         }).ToList();
            
            string retorno = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;
        }
                
    }
}
