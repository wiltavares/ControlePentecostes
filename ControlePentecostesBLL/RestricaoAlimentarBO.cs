﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlePentecostes.DAL;
using Newtonsoft.Json;

namespace ControlePentecostes.BLL
{
    public class RestricaoAlimentarBO
    {

        public string BuscaRestricoes(dynamic param)
        {
            try
            {
                var db = new ControlePentecostesEntities();

                var query = (from p in db.tbl_restricao_alimentar select new { p.nm_restricao_alimentar });

                string retorno = JsonConvert.SerializeObject(query.ToList(), Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int recuperaRestricaoAlimentar(string restricaoSelecionada)
        {
            var db = new ControlePentecostesEntities();

            var query = (from p in db.tbl_restricao_alimentar
                             where p.nm_restricao_alimentar.Equals(restricaoSelecionada)
                             select p).FirstOrDefault();

            if (query == null)
            {
                var restricao = new tbl_restricao_alimentar();
                restricao.nm_restricao_alimentar = restricaoSelecionada;
                db.tbl_restricao_alimentar.Add(restricao);
                db.SaveChanges();

                return restricao.id_restricao_alimentar;
            }

            return query.id_restricao_alimentar;
        }
    }
}
