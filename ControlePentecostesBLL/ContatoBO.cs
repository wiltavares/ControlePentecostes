﻿using ControlePentecostes.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ControlePentecostes.BLL
{
    public class ContatoBO
    {
        public string BuscaContatosPorIdCadastro(dynamic param)
        {
            int idCadastro = Convert.ToInt32(param.where);
            var db = new ControlePentecostesEntities();

            var query = (from c in db.tbl_contatos
                         where c.id_cadastro == idCadastro
                         select new { c.contato });

            string retorno = JsonConvert.SerializeObject(query.ToList(), Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });

            return retorno;
        }
    }
}
