USE [ControlePentecostes]
GO
/****** Object:  Table [dbo].[tbl_cadastro]    Script Date: 14/05/2016 12:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_cadastro](
	[id_cadastro] [int] IDENTITY(1,1) NOT NULL,
	[nm_responsavel] [varchar](150) NOT NULL,
	[informacao_adicional] [varchar](1500) NULL,
	[id_parentesco] [int] NOT NULL,
 CONSTRAINT [PK_tbl_cadastro] PRIMARY KEY CLUSTERED 
(
	[id_cadastro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_contatos]    Script Date: 14/05/2016 12:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_contatos](
	[id_contato] [int] IDENTITY(1,1) NOT NULL,
	[contato] [varchar](150) NOT NULL,
	[id_cadastro] [int] NOT NULL,
 CONSTRAINT [PK_tbl_contatos] PRIMARY KEY CLUSTERED 
(
	[id_contato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_crianca]    Script Date: 14/05/2016 12:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_crianca](
	[id_crianca] [int] IDENTITY(1,1) NOT NULL,
	[nm_crianca] [varchar](150) NOT NULL,
	[idade] [int] NULL,
	[id_restricao_alimentar] [int] NOT NULL,
	[id_grupinho] [int] NOT NULL,
	[id_cadastro] [int] NOT NULL,
	[presente] [int] NULL,
	[filhoDeServo] [int] NULL,
 CONSTRAINT [PK_tbl_crianca] PRIMARY KEY CLUSTERED 
(
	[id_crianca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_grupinho]    Script Date: 14/05/2016 12:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_grupinho](
	[id_grupinho] [int] IDENTITY(1,1) NOT NULL,
	[nm_grupinho] [varchar](150) NOT NULL,
 CONSTRAINT [PK_tbl_grupinho] PRIMARY KEY CLUSTERED 
(
	[id_grupinho] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_parentesco]    Script Date: 14/05/2016 12:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_parentesco](
	[id_parentesco] [int] IDENTITY(1,1) NOT NULL,
	[nm_parentesco] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tbl_parentesco] PRIMARY KEY CLUSTERED 
(
	[id_parentesco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_restricao_alimentar]    Script Date: 14/05/2016 12:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_restricao_alimentar](
	[id_restricao_alimentar] [int] IDENTITY(1,1) NOT NULL,
	[nm_restricao_alimentar] [varchar](150) NOT NULL,
 CONSTRAINT [PK_tbl_restricao_alimentar] PRIMARY KEY CLUSTERED 
(
	[id_restricao_alimentar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbl_cadastro]  WITH CHECK ADD  CONSTRAINT [FK_tbl_cadastro_parentesco] FOREIGN KEY([id_parentesco])
REFERENCES [dbo].[tbl_parentesco] ([id_parentesco])
GO
ALTER TABLE [dbo].[tbl_cadastro] CHECK CONSTRAINT [FK_tbl_cadastro_parentesco]
GO
ALTER TABLE [dbo].[tbl_contatos]  WITH CHECK ADD  CONSTRAINT [FK_tbl_contatos_cadastro] FOREIGN KEY([id_cadastro])
REFERENCES [dbo].[tbl_cadastro] ([id_cadastro])
GO
ALTER TABLE [dbo].[tbl_contatos] CHECK CONSTRAINT [FK_tbl_contatos_cadastro]
GO
ALTER TABLE [dbo].[tbl_crianca]  WITH CHECK ADD  CONSTRAINT [FK_tbl_crianca_cadastro] FOREIGN KEY([id_cadastro])
REFERENCES [dbo].[tbl_cadastro] ([id_cadastro])
GO
ALTER TABLE [dbo].[tbl_crianca] CHECK CONSTRAINT [FK_tbl_crianca_cadastro]
GO
ALTER TABLE [dbo].[tbl_crianca]  WITH CHECK ADD  CONSTRAINT [FK_tbl_crianca_grupinho] FOREIGN KEY([id_grupinho])
REFERENCES [dbo].[tbl_grupinho] ([id_grupinho])
GO
ALTER TABLE [dbo].[tbl_crianca] CHECK CONSTRAINT [FK_tbl_crianca_grupinho]
GO
ALTER TABLE [dbo].[tbl_crianca]  WITH CHECK ADD  CONSTRAINT [FK_tbl_crianca_restricao_alimentar] FOREIGN KEY([id_restricao_alimentar])
REFERENCES [dbo].[tbl_restricao_alimentar] ([id_restricao_alimentar])
GO
ALTER TABLE [dbo].[tbl_crianca] CHECK CONSTRAINT [FK_tbl_crianca_restricao_alimentar]
GO
