USE [master]
GO

/****** Object:  Database [ControlePentecostes]    Script Date: 14/05/2016 12:45:25 ******/
CREATE DATABASE [ControlePentecostes]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ControlePentecostes', FILENAME = N'C:\Users\wilks\bd\ControlePentecostes.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ControlePentecostes_log', FILENAME = N'C:\Users\wilks\bd\ControlePentecostes_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [ControlePentecostes] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ControlePentecostes].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ControlePentecostes] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [ControlePentecostes] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [ControlePentecostes] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [ControlePentecostes] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [ControlePentecostes] SET ARITHABORT OFF 
GO

ALTER DATABASE [ControlePentecostes] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [ControlePentecostes] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [ControlePentecostes] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [ControlePentecostes] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [ControlePentecostes] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [ControlePentecostes] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [ControlePentecostes] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [ControlePentecostes] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [ControlePentecostes] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [ControlePentecostes] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [ControlePentecostes] SET  DISABLE_BROKER 
GO

ALTER DATABASE [ControlePentecostes] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [ControlePentecostes] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [ControlePentecostes] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [ControlePentecostes] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [ControlePentecostes] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [ControlePentecostes] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [ControlePentecostes] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [ControlePentecostes] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [ControlePentecostes] SET  MULTI_USER 
GO

ALTER DATABASE [ControlePentecostes] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [ControlePentecostes] SET DB_CHAINING OFF 
GO

ALTER DATABASE [ControlePentecostes] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [ControlePentecostes] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [ControlePentecostes] SET  READ_WRITE 
GO


