﻿using ControlePentecostes.BLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace ControlePentecostes.WebUi
{
    /// <summary>
    /// Summary description for requisicao
    /// </summary>
    public class requisicao : IHttpHandler
    {
        static string BO_NAMESPACE = "ControlePentecostes.BLL";

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                ParameterHandler param = GetParameterHandler(context);

                if (!string.IsNullOrEmpty(param.nomeBO))
                {
                    Type BOClassType = null;
                    MethodInfo method = null;
                    object BOObject = null;
                    object retorno = null;

                    Assembly assembly = Assembly.Load(BO_NAMESPACE);

                    BOClassType = assembly.GetType(String.Format("{0}.{1}", BO_NAMESPACE, param.nomeBO));
                    BOObject = Activator.CreateInstance(BOClassType);

                    method = BOClassType.GetMethod(param.nomeMetodo);

                    if (method != null)
                    {
                        AttributeBO attrBO = BOClassType.GetCustomAttributes(typeof(AttributeBO), true).FirstOrDefault() as AttributeBO;
                        AttributeBO attrMethod = method.GetCustomAttributes(typeof(AttributeBO), true).FirstOrDefault() as AttributeBO;

                        try
                        {
                            Object[] parameter = new Object[] { param };
                            retorno = BOClassType.InvokeMember(param.nomeMetodo, BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public, null, BOObject, parameter);
                        }
                        catch (Exception ex)
                        {
                            if (ex.InnerException != null)
                            {
                                //TODO - remover stack do alert
                                throw new Exception(ex.InnerException.Message, ex.InnerException);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Método não informado");
                    }

                    if (retorno is List<dynamic> || retorno is IEnumerable<dynamic> || retorno is ExpandoObject)
                        context.Response.Write(JsonConvert.SerializeObject(retorno));
                    else
                        context.Response.Write(retorno);
                }
                else
                {
                    throw new Exception("Service não informado");
                }


            }
            catch (TimeoutException ex)
            {
                throw new Exception("Erro de conexão com o banco de dados. Tente novamente.", ex);
            }  
        }

        private ParameterHandler GetParameterHandler(HttpContext context)
        {
            string nomeBO = string.Empty,
                nomeMetodo = string.Empty;

            this.GetMethodInfo(context, out nomeBO, out nomeMetodo);

            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                string jsonParameter = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();

                JObject jObj = JObject.Parse(jsonParameter);

                ParameterHandler parametro = jObj.ToObject<ParameterHandler>();

                if (parametro != null)
                {
                    parametro.nomeBO = nomeBO;
                    parametro.nomeMetodo = nomeMetodo;
                }

                return parametro;
            }
            else if (!string.IsNullOrEmpty(nomeBO) && !string.IsNullOrEmpty(nomeMetodo))
            {
                return new ParameterHandler() { nomeBO = nomeBO, nomeMetodo = nomeMetodo };
            }
            else
            {
                throw new Exception("ParameterHandler não informado.");
            }
        }

        private void GetMethodInfo(HttpContext context, out string boName, out string methodName)
        {
            boName = string.Empty;
            methodName = string.Empty;

            if (context != null)
            {
                string pathInfo = string.Empty;

                pathInfo = context.Request.PathInfo;

                if (string.IsNullOrEmpty(pathInfo))
                {
                    throw new Exception("Método não informado");
                }

                if (pathInfo.StartsWith("/"))
                {
                    pathInfo = pathInfo.Substring(1);
                }

                string[] path = pathInfo.Split('/');

                boName = (path.Length > 0 ? path[0] : string.Empty);
                methodName = (path.Length > 1 ? path[1] : string.Empty);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class ParameterHandler
    {
        public string nomeBO { get; set; }
        public string nomeMetodo { get; set; }
        public dynamic campos { get; set; }
        public dynamic where { get; set; }
        public string sql { get; set; }
        public string tipoCampo { get; set; }
        public int registroPorPagina { get; set; }
        public int paginaAtual { get; set; }
        public string ordenacao { get; set; }
    }
}