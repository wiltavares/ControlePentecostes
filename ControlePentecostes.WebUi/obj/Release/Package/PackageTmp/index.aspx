﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="ControlePentecostes.WebUi.index" %>

<!DOCTYPE html>
<html lang="en" runat="server">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pentecostes Para Crianças</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/ng-tags-input.min.css" rel="stylesheet" />
    <link href="css/ng-tags-input.bootstrap.min.css" rel="stylesheet" />
    <link href="css/estilo.css" rel="stylesheet" />
    <link href="css/sweetalert.css" rel="stylesheet" />
    <link href="css/mfb.min.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body ng-app="app" ng-controller="IndexCtrl" >
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <%--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">--%>
          <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#/">Pentecostes para Crianças</a>
        </div>
        <div class="collapse navbar-collapse" ng-class="{'in':!navCollapsed}">
            <form class="navbar-form navbar-left" role="search" style="padding-top:6px; margin-left:25%;">
                <div class="form-group">  
                    <span class="label label-primary" ng-cloak style="font-size: 100%;"
                      ng-class="{'label-primary': contCriancas < 80, 'label-warning': contCriancas > 79 && contCriancas < 90, 'label-danger': contCriancas > 89}"
                      > {{ contCriancas }} / {{ capacidadeCriancas }} </span>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right" ng-click="navCollapsed = !navCollapsed">
                
                <li class="active">
                    <a href="#/cadastro">Cadastrar Crianças <span class="sr-only">(current)</span></a>
                </li>
                <li>
                    <a href="#/procurar">Procurar Crianças</a>
                </li>
            </ul>
        </div>
      </div>
    </nav>

    <nav mfb-menu position="br" effect="zoomin" active-icon="glyphicon glyphicon-search" resting-icon="glyphicon glyphicon-search" 
        toggling-method="click" ng-click="sweetInput()"></nav>
    
    <div class="container" style="margin-top:65px;">
        <ng-view></ng-view>
        
    </div>

    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>--%>



    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.js"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.6.0.js" type="text/javascript"></script>
    <script src="http://m-e-conroy.github.io/angular-dialog-service/javascripts/dialogs.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.min.js"></script>

    <script src="js/ng-tags-input.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/mfb-directive.js"></script>


    <script src="https://rawgithub.com/McNull/angular-block-ui/bower/angular-block-ui.min.js"></script>
    <link rel="stylesheet" href="https://rawgithub.com/McNull/angular-block-ui/bower/angular-block-ui.min.css">
    
    <script src="app/app.js"></script>
      
    <%--SERVICES--%>
    <script src="js/ngSweetAlert.min.js"></script>
    <script src="app/services/modalService.js"></script>
    <script src="app/services/businessService.js"></script>

    <%--CONTROLLERS--%>
    <script src="app/Controllers/IndexCtrl.js"></script>
    <script src="app/Controllers/CadastroCtrl.js"></script>
    <script src="app/Controllers/ProcurarCtrl.js"></script>
  </body>
</html>
