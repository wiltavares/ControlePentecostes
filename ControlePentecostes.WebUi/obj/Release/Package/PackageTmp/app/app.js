﻿'use strict';

var app = angular.module('app', ['ui.bootstrap', 'dialogs', 'ngRoute', 'ngTagsInput', 'blockUI', 'hSweetAlert', 'ng-mfb']);


app.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($q, $location, $injector, $rootScope, $window, blockUI) {
        return {

            'responseError': function (response) {
                debugger;
                if (response.status === 420) {
                    $rootScope.alert('erro', response.data.Message);
                
                } else if (response.status === 500) {
                    
                    if (response.data.contains(' The underlying provider failed on Open')) {
                        $rootScope.alert('erro', 'Houve uma falha ao conectar com o banco de dados. Tente novamente!');
                        return;
                    } else {
                        $rootScope.alert('erro', response.data);
                        return;
                    }
                }

                blockUI.stop();

                return $q.reject(response);
            }
        };
    });
});

app.config(function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode({
        enable: true,
        requiredBase: false
    });

    $routeProvider
        .when('/', {
            templateUrl: 'app/views/home.aspx',
        })
        .when('/cadastro', {
            templateUrl: 'app/views/cadastro.aspx'
        })
        .when('/procurar', {
            templateUrl: 'app/views/procurar.aspx'
        })
        .otherwise({
            redirectTo: '/'
        });

});