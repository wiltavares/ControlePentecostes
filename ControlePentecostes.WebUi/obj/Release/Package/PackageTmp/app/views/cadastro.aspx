﻿
<div class="row" ng-controller="CadastroCtrl">
    <div class="col-md-12">
        <form name="itemForm" id="itemForm" novalidate autocomplete="off">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <p class="text-muted">(*) Campos Obrigatórios</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="txtResponsavel" class="text-danger">Responsável (*)</label>
                        <input type="text" ng-model="item.responsavel" class="form-control" id="responsavel" name="responsavel" placeholder="Nome do Responsável" required="required">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="txtParentesco" class="text-danger">Grau de parentesco (*)</label>
                        <input type="text" ng-model="item.parentescoSelecionado" class="form-control" name="parentescoSelecionado" id="parentescoSelecionado"
                            typeahead="parentesco.nm_parentesco for parentesco in parentescos | filter:$viewValue | limitTo:8" placeholder="Pai, Mãe ..." required="required">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="txtContatos" class="text-danger">Contatos (*)</label>
                        <tags-input ng-model="item.contatos" min-tags="0" placeholder="Digite o contato e aperte enter" name="contatos" id="contatos" display-property="contato"></tags-input>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="txtInformacaoAdicional" class="text-danger">Informação Adicional</label>
                        <textarea class="form-control" rows="3" ng-model="item.informacaoAdicional" name="informacaoAdicional" id="informacaoAdicional"
                            placeholder="Informação adicional (Quem pode pegar a criança na saída, etc...)"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="validNomeCrianca">
                        <label for="txtCrianca" class="text-danger">Nome da Criança</label>
                        <input type="text" ng-model="crianca.nome" class="form-control" name="crianca.nome" id="crianca.nome" placeholder="Nome da Criança">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="txtRestricao" class="text-danger">Restrição Alimentar</label>
                        <input type="text" ng-model="crianca.restricaoSelecionada" typeahead="restricao.nm_restricao_alimentar for restricao in restricoes | filter:$viewValue | limitTo:8" 
                            class="form-control" placeholder="Exemplo: Leite, Presunto ..." name="restricaoSelecionada" id="restricaoSelecionada">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="txtIdade" class="text-danger">Idade</label>
                        <input type="number" ng-model="crianca.idade" class="form-control" name="idade" id="idade">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="txtGrupinho" class="text-danger">Grupinho</label>
                        <input type="text" ng-model="crianca.grupinhoSelecionado" class="form-control" placeholder="Nome do Grupinho de Oração"
                            typeahead="grupinho.nm_grupinho for grupinho in grupinhos | filter:$viewValue | limitTo:8" name="grupinhoSelecionado" id="grupinhoSelecionado">
                    </div>
                </div>

                <div class="col-md-2" style="padding-top:20px;">
                    <div class="checkbox">
                        <label class="text-danger"><input type="checkbox" ng-model="crianca.presente">Presente </label>
                    </div>
                </div>
                
                <div class="col-md-2" style="padding-top:20px;">
                    <div class="checkbox">
                        <label class="text-danger"><input type="checkbox" ng-model="crianca.filhoDeServo">Filho de Servo</label>
                    </div>
                </div>

                <div class="col-md-2" style="padding-top:25px;">
                    <div class="form-group pull-right">
                        <label class="text-danger"></label>
                        <button type="button" class="btn btn-success btn-sm" ng-click="addCrianca();">
                            <i class="glyphicon glyphicon-plus"></i>   
                            Adicionar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-success" ng-repeat="x in criancas" style="margin-bottom:5px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="close pull-right" aria-label="Close" ng-click="removerCrianca($index)">
                                        <i class="glyphicon glyphicon-trash text-danger"></i>
                                    </button>
                                    <button type="button" class="close pull-right" aria-label="Close" 
                                        ng-click="entregarCrianca(x)" ng-show="x.presente && x.id_crianca != undefined" style="margin-right:10px;">
                                        <i class="glyphicon glyphicon-ok text-info"></i>
                                    </button>
                                    <button type="button" class="close pull-right" aria-label="Close" 
                                        ng-click="voltarCrianca(x)" ng-show="!x.presente && x.id_crianca != undefined" style="margin-right:10px;">
                                        <i class="glyphicon glyphicon-share-alt text-info"></i>
                                    </button>
                                    <h4 class="list-group-item-heading text-primary">Presente: {{x.presente ? 'Sim' : 'Não'}}, Filho de Servo: {{x.filhoDeServo ? 'Sim' : 'Não'}}</h4>
                                    <h4 class="list-group-item-heading text-primary">Criança: {{ x.nome }} <i ng-show="x.idade != undefined">, {{ x.idade }} anos</i></h4>
                                    <p class="list-group-item-text text-primary" ng-show="x.restricaoSelecionada != undefined">Restrição alimentar: {{ x.restricaoSelecionada }}</p>
                                    <p class="list-group-item-text text-primary" ng-show="x.grupinhoSelecionado">Grupinho de Oração: {{ x.grupinhoSelecionado }}</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-info btn-lg" ng-disabled="disabled()" ng-click="salvar();">
                            <i class="glyphicon glyphicon-floppy-disk"></i>   
                            Salvar
                        </button>
                        <button type="button" class="btn btn-warning btn-lg" ng-click="limpar();">
                            <i class="glyphicon glyphicon-erase"></i>   
                            Limpar
                        </button>
                        <button type="button" class="btn btn-primary btn-lg" ng-click="entregarTodasCriancas(criancas);" ng-disabled="disabled()">
                            <i class="glyphicon glyphicon-ok"></i>   
                            Entregar Crianças
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
