﻿app.controller("CadastroCtrl", ['$scope', '$rootScope', '$dialogs', '$modal', 'Modal', 'bo', 'blockUI', 'sweet', function ($scope, $rootScope, $dialogs, $modal, Modal, bo, blockUI, sweet, $location) {
    $scope.itemForm = {}
    $scope.item = {};
    $scope.crianca = {};
    $scope.criancas = [];
    
    $scope.carregaParentescos = function () {
        blockUI.start();
        bo.postRequisicao('ParentescoBO', 'BuscaParentesco', { where: $scope.item }).then(function (response) {
            
            if (response.data != undefined)
                $scope.parentescos = response.data;
            else
                $scope.parentescos = [];
        });
        blockUI.stop();
    }

    $scope.carregaGrupinhos = function () {
        blockUI.start();
        bo.postRequisicao('GrupinhoBO', 'BuscaGrupinhos', { where: $scope.item }).then(function (response) {
            
            if (response.data != undefined)
                $scope.grupinhos = response.data;
            else
                $scope.grupinhos = [];
        });
        blockUI.stop();
    }

    $scope.carregaRestricoes = function () {
        blockUI.start();
        bo.postRequisicao('RestricaoAlimentarBO', 'BuscaRestricoes', { where: $scope.item }).then(function (response) {
            
            if (response.data != undefined)
                $scope.restricoes = response.data;
            else
                $scope.restricoes = [];
        });
        blockUI.stop();
    }
    
    $scope.buscaCadastroPorId = function (idCadastro) {
        $scope.idCadastro = idCadastro;
        bo.postRequisicao('CadastroBO', 'BuscarCadastroPorId', { where: $scope.idCadastro }).then(function (response) {

            if (response.data != undefined) {
                
                $scope.item = response.data;
                $scope.carregaContatosPorIdCadastro(idCadastro);
                $scope.carregaCriancasPorIdCadastro(idCadastro);
            }
            else {
                return;
            }

        });
    }

    $scope.carregaContatosPorIdCadastro = function (idCadastro) {
        $scope.idCadastro = idCadastro;
        bo.postRequisicao('ContatoBO', 'BuscaContatosPorIdCadastro', { where: $scope.idCadastro }).then(function (response) {

            if (response.data != undefined) {
                $scope.item.contatos =  response.data;
            }
            else {
                return;
            }

        });
    }

    $scope.carregaCriancasPorIdCadastro = function (idCadastro) {
        $scope.idCadastro = idCadastro;
        bo.postRequisicao('CriancaBO', 'BuscaCriancaPorIdCadastro', { where: $scope.idCadastro }).then(function (response) {

            if (response.data != undefined) {
                
                $scope.item.criancas = response.data;
                $scope.criancas = $scope.item.criancas;
            }
            else {
                return;
            }

        });
    }

    $scope.addCrianca = function () {
        if ($scope.crianca == undefined || $scope.crianca.nome == undefined || $scope.crianca.nome == '') {
            Modal.erro('Preencha os campos obrigatórios.');
            document.getElementById('validNomeCrianca').classList.add("has-error");
        }
        else {
            $scope.criancas.push($scope.crianca);
            document.getElementById('validNomeCrianca').classList.remove("has-error");
        }
        $scope.crianca = {};
    }

    $scope.removerCrianca = function ($index) {

        var dlg = null;
        dlg = Modal.aviso('Deseja realmente excluir?', function () {
            $scope.criancas.splice($index, 1);
        });
    }

    $scope.salvar = function () {

        if ($scope.criancas.length <= 0) {
            Modal.erro('Nenhuma criança foi adicionada. Preencha os campos e clique em Adicionar.');
            document.getElementById('validNomeCrianca').classList.add("has-error");
            return;
        }

        itemForm.submitted = true;
        if (itemForm.$invalid) {
            Modal.erro('Preencha todos os campos obrigatórios.');

        } else {
            
            $scope.item.criancas = $scope.criancas;
            blockUI.start();

            if ($scope.item.id_cadastro != undefined) {
                
                bo.postRequisicao('CadastroBO', 'Atualizar', { where: $scope.item }).then(function (response) {

                    if (response.data != undefined) {
                        Modal.confirm("Cadastro atualizado!");                        
                        $scope.item = {};
                        $scope.criancas = [];
                        $rootScope.totalizarCriancas();
                    }
                    else {
                        Modal.erro("Houve uma falha ao atualizar o registro. Atualize a página e tente novamente.");
                    }
                });

            } else {
                bo.postRequisicao('CadastroBO', 'Salvar', { where: $scope.item }).then(function (response) {
                    
                    if (response.data != undefined) {
                        Modal.confirm("Cadastro realizado! Código do cadastro: " + response.data);
                        
                        $rootScope.totalizarCriancas();
                        $scope.item = {};
                        $scope.criancas = [];
                    }
                    else {
                        Modal.erro("Houve uma falha ao salvar o registro. Atualize a página e tente novamente.");
                    }
                });
                
            }
            blockUI.stop();
        }
    }

    $scope.limpar = function () {
        $scope.item = {};
        $scope.criancas = [];
        $scope.crianca = {};
    }

    $scope.disabled = function () {
        
        if ($scope.item.contatos == undefined || $scope.item.responsavel == undefined || $scope.item.parentescoSelecionado == undefined || $scope.criancas.length < 0) {
            return true;
        }
    }

    $scope.entregarCrianca = function (crianca) {
        
        $scope.dados = crianca;
        Modal.aviso('Confirma a entrega da criança?', function () {
            bo.postRequisicao('CriancaBO', 'EntregarCrianca', { where: $scope.dados }).then(function (response) {
                $scope.carregaCriancasPorIdCadastro($scope.dados.id_cadastro);
                $rootScope.totalizarCriancas();
            });
        });
    }

    $scope.entregarTodasCriancas = function (crianca) {        
        $scope.item.where = crianca[0].id_cadastro;

        Modal.aviso('Confirma a entrega de todas as  crianças?', function () {
            bo.postRequisicao('CriancaBO', 'EntregarTodasCriancas', { where: $scope.item }).then(function (response) {
                $scope.carregaCriancasPorIdCadastro(crianca[0].id_cadastro);
                $rootScope.totalizarCriancas();
            });
        });
    }

    $scope.voltarCrianca = function (crianca) {
       
        $scope.dados = crianca;
        Modal.aviso('Confirma a entrada da criança?', function () {
            bo.postRequisicao('CriancaBO', 'VoltarCrianca', { where: $scope.dados }).then(function (response) {
                $scope.carregaCriancasPorIdCadastro($scope.dados.id_cadastro);
                $rootScope.totalizarCriancas();
            });
        });
    }

    $rootScope.initLoad = function () {
        
        $scope.limpar();
        $scope.carregaParentescos();
        $scope.carregaGrupinhos();
        $scope.carregaRestricoes();

        if ($rootScope.codigoCadastro != undefined) {
            $scope.buscaCadastroPorId($rootScope.codigoCadastro);
            $rootScope.codigoCadastro = undefined;
        }
        
    }
    $rootScope.initLoad();

} ]);