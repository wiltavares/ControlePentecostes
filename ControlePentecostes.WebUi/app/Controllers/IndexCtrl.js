﻿app.controller("IndexCtrl", ['$scope', '$rootScope', '$dialogs', '$modal', 'Modal', 'bo', 'blockUI', 'sweet', '$location', function ($scope, $rootScope, $dialogs, $modal, Modal, bo, blockUI, sweet, $location) {
    
    $rootScope.codigoCadastro = undefined;

    $rootScope.alert = function (tipo, mensagem, positiveCallback, negativeCallback, titulo){
        if (tipo == 'confirm'){
            Modal.confirm(mensagem);
        }else if (tipo == 'aviso'){
            Modal.aviso(mensagem);
        }else if (tipo == 'erro'){
            Modal.erro(mensagem);
        }
    }

    $rootScope.sweetInput = function () {
        swal({
            title: "Entregar Criança",
            text: "Digite o código do cadastro:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: ""
        },
        function (inputValue) {
            
            if (inputValue === false) return false;

            if (inputValue === "") {
                swal.showInputError("Digite o código do cadastro.");
                return false
            }

            $rootScope.codigoCadastro = inputValue;            

            swal("", "", "success");
            window.location.href = "#/cadastro";
            $rootScope.initLoad();
        });
    }

    $rootScope.init = function () {
        
        window.location.href = "#/cadastro";
                
        $scope.carregaParentescos();
        $scope.carregaGrupinhos();
        $scope.carregaRestricoes();

        if ($rootScope.codigoCadastro != undefined) {
            $scope.buscaCadastroPorId($rootScope.codigoCadastro);
            $rootScope.codigoCadastro = undefined;
        }
    }

    $rootScope.totalizarCriancas = function () {
        $rootScope.capacidadeCriancas = 100;

        bo.postRequisicao('CriancaBO', 'TotalizarCriancas', {where: null }).then(function (response) {

            if (response.data != undefined) {
                
                $rootScope.contCriancas = response.data;
            }
            else {
                $rootScope.contCriancas = 0;
            }

        });
    }
    $rootScope.totalizarCriancas();

}]);