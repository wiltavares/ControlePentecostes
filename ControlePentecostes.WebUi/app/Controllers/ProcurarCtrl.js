﻿app.controller("ProcurarCtrl", ['$scope', '$rootScope', '$dialogs', '$modal', 'Modal', 'bo', 'blockUI', 'sweet', function ($scope, $rootScope, $dialogs, $modal, Modal, bo, blockUI, sweet, $location) {
    $scope.crianca = {};
    $scope.result = [];

    $scope.buscar = function () {

        bo.postRequisicao('CadastroBO', 'BuscaSelecionada', { where: $scope.crianca }).then(function (response) {

            if (response.data != undefined)
                $scope.result = response.data;
        });
    }

    $scope.CarregarCadastro = function (cadastro) {
        $rootScope.codigoCadastro = cadastro.id_cadastro;
        window.location.href = "#/cadastro";
        $rootScope.init();
    }
}]);