﻿'use strict';

var app = angular.module('app', ['ui.bootstrap', 'dialogs', 'ngRoute', 'ngTagsInput', 'blockUI', 'hSweetAlert', 'ng-mfb']);

//app.config(function ($routeProvider, $locationProvider) {
    
//    $locationProvider.html5Mode({
//        enabled: true,
//        requireBase: false
//    });

//    $routeProvider
//        .when('/ControlePentecostes', {
//            templateUrl: 'app/views/home.aspx'
//        })
//        .when('/ControlePentecostes/cadastro', {
//            templateUrl: 'app/views/cadastro.aspx'
//        })
//        .when('/ControlePentecostes/procurar', {
//            templateUrl: 'app/views/procurar.aspx'
//        })
//        .when('/index.aspx', {
//            redirecTo: '/ControlePentecostes'
//        })
//        .otherwise({
//            redirectTo: '/'
//        });

//});

app.config(function ($routeProvider, $locationProvider) {
    console.log(window.location);
    $routeProvider

    .when("/ControlePentecostes", { templateUrl: "app/views/home.aspx" })

    .when("/index.aspx", { redirectTo: "/ControlePentecostes" })

    .when("/ControlePentecostes/cadastro", { templateUrl: "app/views/cadastro.aspx" })

    .when("/ControlePentecostes/procurar", { templateUrl: "app/views/procurar.aspx" })

    .otherwise({ redirectTo: "/ControlePentecostes" });

    $locationProvider.html5Mode(true);
});

app.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($q, $location, $injector, $rootScope, $window, blockUI) {
        return {

            'responseError': function (response) {
                debugger;
                if (response.status === 420) {
                    $rootScope.alert('erro', response.data.Message);
                
                } else if (response.status === 500) {
                    
                    if (response.data.contains(' The underlying provider failed on Open')) {
                        $rootScope.alert('erro', 'Houve uma falha ao conectar com o banco de dados. Tente novamente!');
                        return;
                    } else {
                        $rootScope.alert('erro', response.data);
                        return;
                    }
                }

                blockUI.stop();

                return $q.reject(response);
            }
        };
    });
});


