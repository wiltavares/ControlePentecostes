app.service('Modal', function ($dialogs, $modal) {

    this.confirm = function (mensagem, positiveCallback, negativeCallback, titulo) {

        var modalInstance = $modal.open({
            animation: true,
            template: '<div class="modal-dialog"><div class="modal-content"><div class="modal-header dialog-header-confirm"><button type="button" class="close" ng-click="no()">×</button><h4 class="modal-title"><span class="glyphicon glyphicon-check"></span> {{ titulo }}</h4></div><div class="modal-body ng-binding"><h5>{{ msg }}</h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" ng-click="ok()">Ok</button></div></div></div>',
            controller: function ($scope, $modalInstance) {
                $scope.titulo = titulo || "Informação!";
                $scope.msg = mensagem;

                $scope.no = function () {
                    $modalInstance.close();
                }

                $scope.ok = function () {
                    $modalInstance.result.then(positiveCallback);
                    $modalInstance.close();
                }
            },
            resolve: {}
        });
    }

  this.aviso = function(mensagem, positiveCallback, negativeCallback, titulo){

      var modalInstance = $modal.open({
        animation: true,
        template: '<div class="modal-dialog"><div class="modal-content"><div class="modal-header dialog-header-wait"><button type="button" class="close" ng-click="no()">×</button><h4 class="modal-title"><span class="glyphicon glyphicon-check"></span> {{ titulo }}</h4></div><div class="modal-body ng-binding"><h5>{{ msg }}</h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" ng-click="ok()">Ok</button><button type="button" class="btn btn-warning" ng-click="no()">Cancelar</button></div></div></div>',
        controller: function ($scope, $modalInstance) {
            $scope.titulo = titulo|| "Aviso!";
  		      $scope.msg = mensagem;

            $scope.no = function(){
              $modalInstance.close();
            }

            $scope.ok = function(){
              $modalInstance.result.then(positiveCallback);
              $modalInstance.close();
            }
  		  },
        resolve: {}
      });
  };

  this.erro = function (mensagem, positiveCallback, negativeCallback, titulo) {

      var modalInstance = $modal.open({
          animation: true,
          template: '<div class="modal-dialog"><div class="modal-content"><div class="modal-header dialog-header-error"><button type="button" class="close" ng-click="no()">×</button><h4 class="modal-title"><span class="glyphicon glyphicon-check"></span> {{ titulo }}</h4></div><div class="modal-body ng-binding"><h5>{{ msg }}</h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" ng-click="ok()">Ok</button></div></div></div>',
          controller: function ($scope, $modalInstance) {
              $scope.titulo = titulo || "Atenção!";
              $scope.msg = mensagem;

              $scope.no = function () {
                  $modalInstance.close();
              }

              $scope.ok = function () {
                  $modalInstance.result.then(positiveCallback);
                  $modalInstance.close();
              }
          },
          resolve: {}
      });
  };

});
