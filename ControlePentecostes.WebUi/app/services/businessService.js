app.factory('bo', ['$q', '$http', '$location', function ($q, $http, $location, blockUI) {
    var url = '/requisicao.ashx';
    //var url = 'http://localhost/ControlePentecostes/requisicao.ashx';

    return {

        getRequisicao: function (bo, operacao, dados) {
            //var dados = { campos: asaus, where: params };
            return $http.get(url + '/' + bo + '/' + operacao, dados);
        },
        postRequisicao: function (bo, operacao, dados) {
            return $http.post(url + '/' + bo + '/' + operacao, dados);
        }
    };
} ]);