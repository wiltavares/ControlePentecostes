﻿

<div class="row" ng-controller="ProcurarCtrl" ng-init="buscar()">
    <div class="col-md-12">
        <form name="itemForm" id="itemForm" novalidate autocomplete="off">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="txtCodigo" class="text-danger">Código:</label>
                        <input type="number" ng-model="crianca.id_cadastro" class="form-control" name="codigo" id="codigo">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="txtCrianca" class="text-danger">Nome da Criança:</label>
                        <input type="text" ng-model="crianca.nm_crianca" class="form-control" name="crianca" id="crianca">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="txtResponsavel" class="text-danger">Nome do Responsável:</label>
                        <input type="text" ng-model="crianca.nm_responsavel" class="form-control" name="responsavel" id="responsavel">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="padding-top:14px;">
                        <label class="text-danger"><input type="checkbox" ng-model="crianca.presente"> Presente</label>   
                        <label class="text-danger"><input type="checkbox" ng-model="crianca.filhoDeServo"> Filho de Servo</label>                     
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-info" ng-click="buscar();">
                            <i class="glyphicon glyphicon-search"></i>   
                            Procurar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Responsável</th>
                                <th>Crianças</th>
                                <th>Presente</th>
                                <th>Filho de Servo</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr ng-repeat="x in result" style="cursor: pointer;" ng-click="CarregarCadastro(x)">
                                    <th>{{x.id_cadastro}}</th>
                                    <td>{{x.nm_responsavel}}</td>
                                    <td>{{x.nm_crianca}}</td>
                                    <td>{{x.presente ? 'Sim' : 'Não'}}</td>
                                    <td>{{x.filhoDeServo ? 'Sim' : 'Não'}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
